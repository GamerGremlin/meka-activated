﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public int currentScore;
    private int highScore;
    public Text scoreDisplay;
    public Text highDisplay;

    // Use this for initialization
    void Start ()
    {
        highScore = PlayerPrefs.GetInt("HighScore");
        highDisplay.text = highScore.ToString();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(currentScore >= highScore)
        {
            highDisplay.text = currentScore.ToString();
        }
        scoreDisplay.text = currentScore.ToString();
	}

    public void AddScore(int score)
    {
        currentScore += score;
    }

    public void OnEnd()
    {
        PlayerPrefs.SetInt("LastScore", currentScore);
    }
}

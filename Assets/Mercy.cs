﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mercy : MonoBehaviour {

    public GameObject lineStart;
    public GameObject lineFinish;
    private LineRenderer line;
    private Vector3 lineStartPos;
    private Vector3 lineFinishPos;
    public GameObject followTarget;
    public float moveSpeed;
    public float maxUp;
    public float maxDown;
    public Health playerHealthScript;
    private int currentTick;
    public int healDelay;

    // Use this for initialization
    void Start ()
    {
        line = gameObject.GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 newVec;
        newVec.x = -7.8f;
        newVec.z = 0f;
        newVec.y = followTarget.transform.position.y;
        transform.position = Vector3.Lerp(transform.position, newVec, moveSpeed);
        lineStartPos = lineStart.transform.position;
        lineFinishPos = lineFinish.transform.position;
        line.SetPosition(0, lineStartPos);
        line.SetPosition(1, lineFinishPos);

        if (currentTick >= healDelay)
        {
            playerHealthScript.currentHealth++;
            currentTick = 0;
        }
        else
        {
            currentTick++;
        }
	}
}

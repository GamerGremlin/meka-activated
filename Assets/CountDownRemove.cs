﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountDownRemove : MonoBehaviour {

    public int disableAt;
    private int currentTick;

	// Use this for initialization
	void Start ()
    {
        currentTick = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        currentTick++;
        if(currentTick >= disableAt)
        {
            currentTick = 0;
            gameObject.SetActive(false);
        }
	}
}

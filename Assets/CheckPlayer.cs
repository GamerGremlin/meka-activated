﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPlayer : MonoBehaviour {

    public GameObject Player;
    public StartGame script;
    public Score scoreScript;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Player == null)
        {
            scoreScript.OnEnd();
            script.StartCounting();
            script.PlaySound();
        }
	}
}

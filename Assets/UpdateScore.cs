﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UpdateScore : MonoBehaviour {

    private int readScore;
    public int inputScore;
    public GameObject toEnable;

	// Use this for initialization
	void Start ()
    {
        readScore = PlayerPrefs.GetInt("HighScore");
        inputScore = PlayerPrefs.GetInt("LastScore");
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    bool CheckScore()
    {
        if(readScore < inputScore)
        {
            readScore = inputScore;
            PlayerPrefs.SetInt("HighScore", readScore);
            toEnable.SetActive(true);
            return true;
        }
        else
        {
            return false;
        }
    }
}

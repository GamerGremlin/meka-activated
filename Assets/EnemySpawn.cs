﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {

    public int interval;
    private int currentInterval;
    public GameObject toSpawn;
    public GameObject scoreScript;
    public GameObject maxY;
    public GameObject minY;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(currentInterval < interval)
        {
            currentInterval++;
        }
        else
        {
            GameObject spawn = Instantiate(toSpawn, new Vector3(transform.position.x, Random.Range(minY.transform.position.y, maxY.transform.position.y), transform.position.z), toSpawn.transform.rotation, null);
            spawn.GetComponent<Health>().scoreScript = scoreScript;
            currentInterval = 0;
        }
	}
}

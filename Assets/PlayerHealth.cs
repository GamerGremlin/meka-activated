﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    private Health playerHealthScript;
    public GameObject healthBar;

	// Use this for initialization
	void Awake ()
    {
        playerHealthScript = gameObject.GetComponent<Health>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        healthBar.GetComponent<Image>().fillAmount = playerHealthScript.currentHealth;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerFire : MonoBehaviour {

    public GameObject PrimaryProjectile;
    public GameObject SecondaryProjectile;
    public KeyCode Fire;
    public KeyCode Secondary;
    public KeyCode Matrix;
    private Transform PrimarySpawn;
    private Transform SecondarySpawn;
    public float PrimaryCooldown;
    public float SecondaryCooldown;
    public float MatrixCooldown;
    private float CurrentPrimaryCD;
    private float CurrentSecondaryCD;
    private float CurrentMatrixCD;
    public Image MatrixCD;
    public Image MissileCD;

    // Use this for initialization
    void Start ()
    {
        PrimarySpawn = transform.Find("PrimarySpawn");
        SecondarySpawn = transform.Find("SecondarySpawn");
	}
	
	// Update is called once per frame
	void Update ()
    {
        UpdateCooldowns();
        if (Input.GetKey(Fire) && PrimaryCooldown ==  CurrentPrimaryCD)
        {
            Instantiate(PrimaryProjectile, PrimarySpawn.position, PrimaryProjectile.transform.rotation, null);
            CurrentPrimaryCD = 0;
        }
        if(Input.GetKey(Secondary) && CurrentSecondaryCD == SecondaryCooldown)
        {
            Instantiate(SecondaryProjectile, SecondarySpawn.position, SecondaryProjectile.transform.rotation, null);
            CurrentSecondaryCD = 0;
        }
        if(Input.GetKey(Matrix) && CurrentMatrixCD == MatrixCooldown)
        {
            foreach(GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
            {
                Destroy(enemy);
            }
            CurrentMatrixCD = 0;
        }
	}

    void UpdateCooldowns()
    {
        if(CurrentMatrixCD < MatrixCooldown)
        CurrentMatrixCD++;
        if(CurrentPrimaryCD < PrimaryCooldown)
        CurrentPrimaryCD++;
        if(CurrentSecondaryCD < SecondaryCooldown)
        CurrentSecondaryCD++;
    }
}

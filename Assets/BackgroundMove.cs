﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMove : MonoBehaviour {

    public GameObject gotoPos;
    public float speed;
    private Transform gotoPosT;

	// Use this for initialization
	void Awake ()
    {
        gotoPosT = gotoPos.transform;
	}
	
	// Update is called once per frame
	void Update ()
    {
        gameObject.transform.Translate(Vector3.left * Time.deltaTime * speed);
	}

    void OnBecameInvisible()
    {
        gameObject.transform.position = gotoPosT.position;
    }
}

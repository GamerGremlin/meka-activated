﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour {

    public AudioSource audioPlayer;
    private bool isCounting;
    private int count;
    public int countTo;
    public int SceneID;

	// Use this for initialization
	void Start ()
    {
        count = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (isCounting)
        {
            count++;
        }
        if(count >= countTo)
        {
            SceneManager.LoadScene(SceneID, LoadSceneMode.Single);
        }
	}

    public void StartCounting()
    {
        isCounting = true;
    }

    public void PlaySound()
    {
        audioPlayer.Play();
    }
}

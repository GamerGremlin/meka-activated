﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MercyActivate : MonoBehaviour {

    public GameObject ToActivate;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            ToActivate.SetActive(true);
            Destroy(this.gameObject);
        }
    }
}

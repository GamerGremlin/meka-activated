﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

    public KeyCode Up;
    public KeyCode Down;
    public float maxDown = -3f;
    public float maxUp = 3.5f;
    public float moveSpeed = 0.5f;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKey(Up) && transform.position.y < maxUp)
        {
            transform.Translate(transform.up * moveSpeed);
        }
        if (Input.GetKey(Down) && transform.position.y > maxDown)
        {
            transform.Translate(-transform.up * moveSpeed);
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour {

    public float moveSpeed;
    public int damage;
    private bool stopped;
    private bool ticking;
    public int tickTo;
    private int ticker;

	// Use this for initialization
	void Start ()
    {
        stopped = false;
        ticking = false;
	}
	
	// Update is called once per frame
	void Update ()
    {

        if(!stopped && ticking)
        {
            ticker++;
            if(ticker >= tickTo)
            {
                stopped = true;
                ticking = false;
            }
        }
        else
        {
            transform.Translate(transform.right * moveSpeed);
        }
	}

    void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<Health>().DealDamage(damage);
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!stopped)
        {
            ticking = true;
        }
    }
}

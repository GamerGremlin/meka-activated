﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour {

    public int maxHealth;
    public int currentHealth;
    public GameObject scoreScript;
    public int killScore;

	// Use this for initialization
	void Start ()
    {
        currentHealth = maxHealth;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(currentHealth <= 0)
        {
            if (scoreScript != null)
            {
                scoreScript.GetComponent<Score>().AddScore(killScore);
            }
            Destroy(this.gameObject);
        }

        if(currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
	}

    public void DealDamage(int dealDamage)
    {
        currentHealth -= dealDamage;
    }
}
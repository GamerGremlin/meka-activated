﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainProjec : MonoBehaviour {

    public float moveSpeed;
    public int damage;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Translate(-transform.right * moveSpeed);
	}
    
    void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Player")
        {
            collision.gameObject.GetComponent<Health>().DealDamage(damage);
            Destroy(this.gameObject);
        }
    }
}
